/* eslint-disable */
import gulp from 'gulp';
import gulpSass from 'gulp-sass';
import gulpAutoprefixer from 'gulp-autoprefixer';
import gulpNunjucksRender from 'gulp-nunjucks-render';
import { create as browserSyncCreate } from 'browser-sync';
import del from 'del';
import gulpCssnano from 'gulp-cssnano';
import gulpSourcemaps from 'gulp-sourcemaps';
import browserify from 'browserify';
import vinylSourceStream from 'vinyl-source-stream';
import vinylBuffer from 'vinyl-buffer';
import babelify from 'babelify';
import gulpSvgSprites from 'gulp-svg-sprites';
import gulpTerser from 'gulp-terser';

const browserSync = browserSyncCreate();
const browserSyncReload = browserSync.reload;

// Clean dist directory.
gulp.task('clean', () => {
	return del(['dist']);
});

// Compile SCSS to CSS.
gulp.task('scss-compile', () => {
	return gulp.src('./src/static/scss/all.scss')
		.pipe(gulpSourcemaps.init())
		.pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(gulpAutoprefixer({
			grid: false
		}))
		.pipe(gulpCssnano())
		.pipe(gulpSourcemaps.write('.'))
        .pipe(gulp.dest('./dist/static/css/'))
        .pipe(browserSyncReload({ stream: true }));
});

// Transpile, bundle and minify JavaScript.
gulp.task('js-main-compile', () => {
	const bundler = browserify({
		entries: './src/static/js/main.js',
		debug: true
	});

	return bundler
		.transform(babelify)
		.bundle()
		.pipe(vinylSourceStream('main.js'))
		.pipe(vinylBuffer())
		.pipe(gulpSourcemaps.init({ loadMaps: true }))
		.pipe(gulpTerser())
		.pipe(gulpSourcemaps.write('.'))
		.pipe(gulp.dest('./dist/static/js/'))
		.pipe(browserSyncReload({ stream: true }));
});

// Compile JavaScript polyfill.
gulp.task('js-polyfill-compile', done => {
	browserify('./src/static/js/polyfill.js')
		.transform(babelify)
		.bundle()
		.pipe(vinylSourceStream('polyfill.js'))
		.pipe(gulp.dest('./dist/static/js/'));

	done();
});

// Compile HTML.
gulp.task('html-compile', () => {
	return gulp.src(['./src/**/templates/*.njk', './src/**/index.njk'])
		.pipe(gulpNunjucksRender({
			path: ['./src/templates/', './src/layout/', './src/components/']
		}))
		.pipe(gulp.dest('./dist/'))
		.pipe(browserSyncReload({ stream: true }));
});

// Compile SVG icons to an SVG sprite inside the src directory. This will be copied to the dist directory by the 'img-copy' task.
gulp.task('sprite-compile', () => {
	return gulp.src('./src/static/img/svg/!(sprite)*.svg')
        .pipe(gulpSvgSprites({
			templates: {
				scss: require('fs').readFileSync('sprite-template.scss', 'utf-8')
			},
			selector: 'icon-%f',
			cssFile: '../scss/config/_icons.scss', // generated
			svgPath: '../img/%f',
			preview: false
		}))
        .pipe(gulp.dest('./src/static/img/'));
});

// Copy img directory.
gulp.task('img-copy', () => {
	return gulp.src('./src/static/img/**/*.*')
		.pipe(gulp.dest('./dist/static/img/'));
});

// Start local development server.
gulp.task('browsersync', done => {
	browserSync.init({
		server: {
			baseDir: 'dist'
		},
		notify: false,
		open: false
	});
	
	done();
});

gulp.task('watch', () => {
	gulp.watch('./src/**/*.scss', gulp.parallel(['scss-compile']));

	gulp.watch(['./src/**/*.js', '!./src/static/js/polyfill.js'], gulp.parallel(['js-main-compile']));
	gulp.watch('./src/static/js/polyfill.js', gulp.parallel(['js-polyfill-compile']));

	gulp.watch('./src/**/*.njk', gulp.parallel(['html-compile']));
});

gulp.task('dev', gulp.series(
	'clean',
	'scss-compile',
	'js-main-compile',
	'js-polyfill-compile',
	'sprite-compile',
	'html-compile',
	'img-copy',
	'browsersync',
	'watch'
));
