# Requirements - globally installed
* [Node.js](https://nodejs.org/en/).
* [npm](https://www.npmjs.com/) (included in Node.js).
* [gulp](https://gulpjs.com/docs/en/getting-started/quick-start).

# How-to
* Install packages: `npm i`.
* Start local development environment: `gulp dev`.

# About
This is a local frontend development environment used to speed up the development of a new project. It includes the following techniques:

## gulp
[gulp](https://gulpjs.com/) is used as a build system.

## Browsersync
[Browsersync](https://www.browsersync.io/) creates a local development server with live file reloading.

## HTML
HTML is written in the templating engine [nunjucks](https://mozilla.github.io/nunjucks/). HTML files are separated as layout, component and template. A template can extend a layout and can include components.

## CSS
CSS can be written in multiple, reusable files using [SCSS](http://sass-lang.com/) which will be compiled and minified to one CSS file: `all.css`. Make sure to add the SCSS files to `src/static/scss/all.scss`. Vendor prefixes will be added during compilation by [Autoprefixer](https://github.com/postcss/autoprefixer). Source maps are also generated.

## JavaScript
JavaScript can be written in multiple, reusable modules which will be bundled with [Browserify](http://browserify.org/) and minified to one JavaScript file: `main.js`. It can be written in modern syntax (ES2015+), which will be transpiled with [Babel](https://babeljs.io/) to ES5. Source maps are also generated.

Take a look at the HTML and JavaScript in `src/components/introduction/` to see how a class (as a module module) can be linked to an HTML component. When this component is being viewed on a webpage, the module will be called by `src/static/js/main.js`, supplying the element itself and the options specified on the element.

When you create a new module, add it to `src/static/js/main.js` too.

[core-js](https://github.com/zloirock/core-js) is used to include polyfills in `src/static/js/polyfill.js`. Only include the polyfills you need. The compiled `polyfill.js` is only loaded when the browser lacks support for one or more specified techniques. This check needs to be done in `src/layout/layout.html`.

## Browserslist
A `.browserslistrc` file describes the [supported browsers](https://browserl.ist/). This is used by Babel and Autoprefixer.

## Linting
When using Git, staged JavaScript and CSS will automatically be linted before committing them using [lint-staged](https://github.com/okonet/lint-staged). Some linting errors are automatically fixed using `stylefmt` and ESLint's `--fix` option. This can be adjusted in `package.json`.

## Font loading
With [Font Face Observer](https://github.com/bramstein/fontfaceobserver), font loading is managed. On first page visit, a fallback font will be shown until the specified font is loaded to prevent the ["Flash of Invisible Text"](https://www.zachleat.com/web/comprehensive-webfonts/). When the user re-visits the page, the correct font is immediately shown. See the code in `src/static/js/main.js`, `src/static/css/common/_typography.scss` and `src/layout/index.html`.

## SVG icon sprite system
With [gulp-svg-sprites](https://www.npmjs.com/package/gulp-svg-sprites), all `.svg` files in `src/static/img/svg/` will be bundled to a sprite. CSS is automatically generated with the code needed to use the sprite. The file `src/static/scss/config/_icons.scss` is dynamically generated using the file `sprite-template.scss`. Currently, when adding a new svg, `gulp dev` needs to be ran again. 

An icon can be used in two ways (the name of the original `.svg` icon will also be the classname, prepended by `icon-`):

```
<span class="icon-twitter"></span>

<span class="my-icon"></span>
.my-icon {
	@include icon;
	@include icon-twitter;
}
```
