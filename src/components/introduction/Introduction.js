export default class {
	/**
	* @param {HTMLElement} element
	* @param {Object} options
	*/
	constructor(element, options) {
		this._element = element;
		this._options = options;

		this._showMessage();
	}

	_showMessage() {
		const message = this._options.message;
		const paragraph = document.createElement('p');
		paragraph.innerHTML = message;

		this._element.appendChild(paragraph);
	}
}
