module.exports = {
	"env": {
		"browser": true,
		"es6": true
	},
	"parserOptions": {
		"ecmaVersion": 2019,
		"sourceType": "module"
	},
	"extends": "eslint:recommended",
	"rules": {
		"indent": [
			"error",
			"tab"
		],
		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"single"
		],
		"semi": [
			"error",
			"always"
		]
	}
};
